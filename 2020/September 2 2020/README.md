# Alpha Anywhere Webcast September 2, 2020

[Watch Webinar](https://youtu.be/tMgSQHo-660)

## Downloading only the component for this webinar:

1. Right click the download link below
2. Select "Save link as"
3. Save the file

<a href="https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2020/September%202%202020/visibility_demonstration_ux.a5wcmp">Download visibility_demonstration_ux.a5wcmp</a>
