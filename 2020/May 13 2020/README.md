# Alpha Anywhere Webcast 2020 May 13 - Tabbed UI Replacement Part 2

[Watch Webinar](https://youtu.be/YKE3-VKpcvU)

## Downloading only the files for this webinar:

<a href="https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2020/May%2013%202020/tabbed_ui_replacement.zip">Download tabbed_ui_replacement.zip</a>
