# Alpha Anywhere Webcast 2022 March 16 - Adding Authentication to your Webservice Part 2

[Watch Webinar](https://www.youtube.com/watch?v=Stgwpocb6gM)

## Download Presentation
 - [AuthenticationAPI2.pptx](https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2022%2003%2016/AuthenticationAPI2.pptx)
 - [AuthenticationAPI2.pdf](https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2022%2003%2016/AuthenticationAPI2.pdf)

## Downloading the component for this webinar

Coming SOON!
