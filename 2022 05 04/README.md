# Alpha Anywhere Webcast 2022 May 4 - Security Framework - New additions to the Quick Setup Genie

[Watch Webinar](https://www.youtube.com/watch?v=EJ95I3jja-4)

## Download Presentation
 - [UsersAndGroups.pptx](https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2022%2005%2004/UsersAndGroups.pptx)
 - [UsersAndGroups.pdf](https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2022%2005%2004/UsersAndGroups.pdf)

## Useful Resources
Coming Soon

## Downloading the component for this webinar

There are no components for this webinar.
