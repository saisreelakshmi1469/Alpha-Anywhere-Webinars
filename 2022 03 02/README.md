# Alpha Anywhere Webcast 2022 March 2 - Branding

[Watch Webinar](https://www.youtube.com/watch?v=oIINPJxVOtY)

## Download Presentation
 - [BrandingOverview.pptx](https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2022%2003%2002/BrandingOverview.pptx)
 - [BrandingOverview.pdf](https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2022%2003%2002/BrandingOverview.pdf)

## Useful Resources
These articles contain a wealth of information on how to use Alpha Anywhere to modify your application to fit your brand.

 - [Alpha Web Theme Builder](https://documentation.alphasoftware.com/documentation/index?search=stylesheet%20builder)
 - [Inherited Styles](https://documentation.alphasoftware.com/documentation/index?search=sass%20inherited%20styles)
 - [Sub-themes](https://documentation.alphasoftware.com/documentation/index?search=subthemes%20in%20alpha)
 - [Customizing Colors and Fonts](https://documentation.alphasoftware.com/documentation/index?search=customizing%20style%20colors%20and%20fonts)
 - [The Pulse Effect](https://documentation.alphasoftware.com/documentation/index?search=adding%20pulse%20effects)
 - [How to Add a Class and Reference it in a Control](https://documentation.alphasoftware.com/documentation/index?search=howto%20addcontrolclass)
 - [How to Add Local and Global Shared Styles](https://documentation.alphasoftware.com/documentation/index?search=howto%20addsharedstyles)
 - [How to Create and Assign a Sub-theme](https://documentation.alphasoftware.com/documentation/index?search=howto%20createsubtheme)
 - [How to Add a Web Font to Your Project](https://documentation.alphasoftware.com/documentation/index?search=howto%20add%20a%20webfont)
 - [How to Configure the Project Style to use the Compact or Pulse Options](https://documentation.alphasoftware.com/documentation/index?search=howto%20configure%20project%20style%20compact%20pulse)
 - [How to Define Style Tweaks](https://documentation.alphasoftware.com/documentation/index?search=howto%20definestyletweaks)
 - [How to Preview Themes in Other Contexts in the Web Theme Builder](https://documentation.alphasoftware.com/documentation/index?search=howto%20preview%20themes%20in%20other%20contexts)
 - [How to Set the Height of a Container to the Screen Height](https://documentation.alphasoftware.com/documentation/index?search=howto%20set%20height%20to%20screen%20height)

## Downloading the component for this webinar

There are no components for this webinar.
