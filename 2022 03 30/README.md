# Alpha Anywhere Webcast 2022 March 30 - Introduction to "Offline" Apps

[Watch Webinar](https://www.youtube.com/watch?v=--uywhBc1IM)

## Downloading the component for this webinar

1. Right click the download link below
2. Select "Save link as"
3. Save the file

[Download offline_and_viewbox_component.zip](https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2022%2003%2030/offline_and_viewbox_component.zip)
