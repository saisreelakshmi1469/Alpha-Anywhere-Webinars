# Alpha Anywhere Webcast 2022 April 20 - Indexed DB - Client side data storage & List Control

[Watch Webinar](https://youtu.be/i16ybxWzN0I)

## Downloading the component for this webinar

1. Right click the download link below
2. Select "Save link as"
3. Save the file

[Download indexeddb_example.zip](https://github.com/alphaanywhere/Alpha-Anywhere-Webinars/raw/master/2022%2004%2020/indexeddb_example.zip)
